import React, {Component} from 'react';
import './Team.css'

class Team extends Component{
    state = {
        score : 0
    }

    increaseScore = () =>{
        this.setState({
            score: this.state.score + 2
        });
    }

     render () {
       return (
        <div className="Team">
         <h2 onClick={this.props.click}>{this.props.bibi}</h2>
         <input type='text' onChange={this.props.Change}/>
         <h1>{this.state.score}</h1>
         <button onClick={this.increaseScore}>+2</button> 
         </div> 
       );
     }
  }

  export default Team;
  