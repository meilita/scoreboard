import React, {Component} from 'react';
import './App.css';
import Team from './Team';

class App extends Component {
  state = {
    teams_name : ["Hawks", "Eagles", "Lakers", "Raptors", "Warriors", "Bulls"],
    teams: [
      {name : "Hawks"},
      {name : "Eagles"} //object
    ]
  }

  switchTeamOne = () => {
    this.setState({
      teams: [
        {name: this.state.teams_name[Math.floor(Math.random() * this.state.teams_name.length)]},
        {name: this.state.teams[1].name}
      ]
    })
  }

  switchTeamTwo = () => {
    this.setState({
      teams: [
        {name: this.state.teams[0].name},
        {name: this.state.teams_name[Math.floor(Math.random() * this.state.teams_name.length)]}
      ]
    })
  }

    switchByInput = (event) => {
      this.setState({
        teams: [
          {name: event.target.value},
          {name: event.target.value}
        ]
      })
    }


   render(){
     return (
       <div className="App">
         <Team 
            bibi = {this.state.teams[0].name} />
        <button className="Button" onClick={this.switchTeamOne}>Switch Team</button>       
         <Team 
            bibi = {this.state.teams[1].name} 
            change = {this.switchByInput
          }/>
        <button className="Button" onClick={this.switchTeamTwo}>Switch Team</button>    
       </div>
     );
   }
 }

export default App;

